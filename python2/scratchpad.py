import os
import shutil

import master_helper

overwrite_maps = True

main_directory_path = master_helper.get_main_directory_path()
# fits
fit_directory_path = os.path.join(main_directory_path, "DMD_fit_20_20")
fit_phase_map_txt_fname = os.path.join(fit_directory_path, "phaseMap_interpolated.txt")
fit_phase_map_png_fname = os.path.join(fit_directory_path, "phaseMap_interpolated.png")
fit_amplitude_map_txt_fname = os.path.join(fit_directory_path, "amplitudeMap_interpolated.txt")
fit_amplitude_map_png_fname = os.path.join(fit_directory_path, "amplitudeMap_interpolated.png")

# map location
maps_directory_path = os.path.join(main_directory_path, "maps")
actual_phase_map_txt_fname = os.path.join(maps_directory_path, "phaseMap_interpolated.txt")
actual_phase_map_png_fname = os.path.join(maps_directory_path, "phaseMap_interpolated.png")
actual_amplitude_map_txt_fname = os.path.join(maps_directory_path, "amplitudeMap_interpolated.txt")
actual_amplitude_map_png_fname = os.path.join(maps_directory_path, "amplitudeMap_interpolated.png")

if overwrite_maps:
    # shutil.copyfile(fit_phase_map_txt_fname, actual_phase_map_txt_fname)
    # shutil.copyfile(fit_phase_map_png_fname, actual_phase_map_png_fname)
    shutil.copyfile(fit_amplitude_map_txt_fname, actual_amplitude_map_txt_fname)
    shutil.copyfile(fit_amplitude_map_png_fname, actual_amplitude_map_png_fname)
