# General
import numpy as np
import scipy
import os
import cPickle as pickle

# Specific
import holo_maker
import DMDwrapper
import master_helper


##############################################################################
def initHolomaker(verbose=False):

    MAXNUMROWS = 684
    MAXNUMCOLS = 608

    #phaseMap
    main_directory_path = master_helper.get_main_directory_path()
    maps_directory_path = os.path.join(main_directory_path, "maps")
    with open(os.path.join(maps_directory_path, "phaseMap_interpolated.txt"), 'r') as f:
        phaseMap_interpolated = pickle.load(f)
    phaseMap_interpolated = np.flipud(phaseMap_interpolated)

    #amplitudeMap
##    phaseMap_interpolated = np.zeros((MAXNUMROWS,MAXNUMCOLS))+0.0
    amplitudeMap_interpolated = np.zeros((MAXNUMROWS,MAXNUMCOLS))+1.0


    #create blank
    a = 2 #sharpness
    d = 4 #carrier wave period in pixels
    angle_deg = 20.0 # carrier wave direction relative to horizontal (degrees)
    if (verbose):
        print "Initializating holomaker"
    holomaker = holo_maker.holo_maker(0, \
                                      phaseMap_interpolated, \
                                      amplitudeMap_interpolated, \
                                      sharpness=a, \
                                      period=d, \
                                      angle=angle_deg)
    return holomaker


def makeHG(waistX, waistY, nx, ny, phi, epsilon_phi, center, verbose=False, save=0):

    MAXNUMROWS = 684
    MAXNUMCOLS = 608

    #phaseMap
    main_directory_path = master_helper.get_main_directory_path()
    maps_directory_path = os.path.join(main_directory_path, "maps")
    with open(os.path.join(maps_directory_path, "phaseMap_interpolated.txt"), 'r') as f:
        phaseMap_interpolated = pickle.load(f)
    phaseMap_interpolated = np.flipud(phaseMap_interpolated)

    #amplitudeMap
##    phaseMap_interpolated = np.zeros((MAXNUMROWS,MAXNUMCOLS))+0.0
    amplitudeMap_interpolated = np.zeros((MAXNUMROWS,MAXNUMCOLS))+1.0


    #create blank
    a = 2 #sharpness
    d = 4 #carrier wave period in pixels
    angle_deg = 20.0 # carrier wave direction relative to horizontal (degrees)
    if (verbose):
        print "Initializating holomaker"
    holomaker = holo_maker.holo_maker(0, \
                                      phaseMap_interpolated, \
                                      amplitudeMap_interpolated, \
                                      sharpness=a, \
                                      period=d, \
                                      angle=angle_deg)

    #add HG
    if (verbose):
        print "Adding HG"
    holomaker.addHG(waistX, waistY, nx, ny, phi, epsilon_phi, center, 0)
    if (verbose):
        print "making hologram"
    hologram = holomaker.makeHologram()

    #save
    saveTo =  os.path.join(main_directory_path, "test")
    hologram_path = os.path.join(saveTo, "hologram"+str(save)+".bmp")
    scipy.misc.imsave(hologram_path, hologram)
    
    #display on DMD
    DMDwrapper.display_bitmap(hologram_path,1)
    return hologram_path



def makeLG(waist, n1, n2, phi, epsilon_phi, center):

    MAXNUMROWS = 684
    MAXNUMCOLS = 608

    #phaseMap
    main_directory_path = master_helper.get_main_directory_path()
    maps_directory_path = os.path.join(main_directory_path, "maps")
    with open(os.path.join(maps_directory_path, "phaseMap_interpolated.txt"), 'r') as f:
        phaseMap_interpolated = pickle.load(f)
    phaseMap_interpolated = np.flipud(phaseMap_interpolated)
    #phaseMap_interpolated = np.zeros((MAXNUMROWS,MAXNUMCOLS))+0.0
    #amplitudeMap
    #amplitudeMap_interpolated = np.zeros((MAXNUMROWS,MAXNUMCOLS))+1.0
    with open(os.path.join(maps_directory_path, "amplitudeMap_interpolated.txt"), 'r') as f:
        amplitudeMap_interpolated = pickle.load(f)
    #amplitudeMap_interpolated = np.flipud(amplitudeMap_interpolated)
    


    #create blank
    a = 2 #sharpness
    d = 4 #carrier wave period in pixels
    angle_deg = 20.0 # carrier wave direction relative to horizontal (degrees)
    holomaker = holo_maker.holo_maker(0, \
                                      phaseMap_interpolated, \
                                      amplitudeMap_interpolated, \
                                      sharpness=a, \
                                      period=d, \
                                      angle=angle_deg)

    #add HG
    holomaker.addLG(waist, n1, n2, phi, epsilon_phi, center, 0)
    hologram = holomaker.makeHologram()

    #save
    saveTo =  os.path.join(main_directory_path, "test")
    hologram_path = os.path.join(saveTo, "hologram.bmp")
    scipy.misc.imsave(hologram_path, hologram)

    #display on DMD
    DMDwrapper.display_bitmap(hologram_path,1)
    return hologram_path


def makeCirc(waist, n1, n2, phi, epsilon_phi, center):

    MAXNUMROWS = 684
    MAXNUMCOLS = 608

    #phaseMap
    main_directory_path = master_helper.get_main_directory_path()
    maps_directory_path = os.path.join(main_directory_path, "maps")
    #with open(os.path.join(maps_directory_path, "phaseMap_interpolated.txt"), 'r') as f:
    #     phaseMap_interpolated = pickle.load(f)
    #phaseMap_interpolated = np.flipud(phaseMap_interpolated)
    phaseMap_interpolated = np.zeros((MAXNUMROWS,MAXNUMCOLS))+0.0
    #amplitudeMap
    amplitudeMap_interpolated = np.zeros((MAXNUMROWS,MAXNUMCOLS))+1.0


    #create blank
    a = 2 #sharpness
    d = 4 #carrier wave period in pixels
    angle_deg = 20.0 # carrier wave direction relative to horizontal (degrees)
    holomaker = holo_maker.holo_maker(0, \
                                      phaseMap_interpolated, \
                                      amplitudeMap_interpolated, \
                                      sharpness=a, \
                                      period=d, \
                                      angle=angle_deg)

    #add HG
    holomaker.addCircMode(waist, n1, n2, center, 0)
    hologram = holomaker.makeHologram()

    #save
    saveTo =  os.path.join(main_directory_path, "test")
    hologram_path = os.path.join(saveTo, "hologram.bmp")
    scipy.misc.imsave(hologram_path, hologram)

    #display on DMD
    DMDwrapper.display_bitmap(hologram_path,1)
    return hologram_path

def run():
    print "connecting"
    nx=1
    ny=1
    w=100
    hologram_path1 = makeHG(w,w, nx,ny ,0,0, (-80,0),verbose=True,save=1)
    hologram_path2 = makeHG(w,w, nx,ny ,0,0, (80,0),verbose=True,save=2)
    while (True):
        DMDwrapper.display_bitmap(hologram_path1,1)
        DMDwrapper.display_bitmap(hologram_path2,1)

if __name__ == "__main__":
    #run()
    print "loaded"
    nR = 0
    nL = 0
    nx=min(nR,nL)
    ny=nR-nL
    n1=1
    n2=1
    w=32
    hologram_path1 = makeHG(w,w, n1,n2 ,0,0, (0,0),verbose=True,save=1)
    #hologram_path1 = makeLG(w, nx,ny ,0,0, (0,0))
    #hologram_path1 = makeCirc(w, nx,ny ,0,0, (0,0))    
    #DMDwrapper.display_bitmap(hologram_path1,1)
