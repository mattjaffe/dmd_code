'''

Pre-calculate holograms for phase mapping / calibration of the DMD
(see Chapter 3 of Philip Zupancic's Master's thesis)

- Divide the DMD into patches and interfere them with each other to find their relative phase

'''

# To-do:
# move hologram parameters outside of makeOneHologram function
# add option to patch function for Bool (in addition to LG) ???

import numpy as np
import os
import time
import pickle
from joblib import Parallel, delayed
from PIL import Image

import helper_funcs
import holo_maker

def make_the_holograms(numRows, numCols, dmd,
                        phase_correct=False, amplitude_correct=False, verbose=True, numPhases=3,
                        w=10, n1=0, n2=0, a=2, d=4, angle_deg=20.,
                        main_dir=None, maps_dir=None, hologram_dir=None, holo_dir_mod=''):
    # Directories
    if main_dir is None:
        main_dir = os.path.join(helper_funcs.get_main_directory_path(),'calibration')
    if maps_dir is None:
        maps_dir = os.path.join(helper_funcs.get_main_directory_path(), 'maps')
    if hologram_dir is None:
        hologram_dir = os.path.join(main_dir, f'{holo_dir_mod}DMD_hologram_{numRows}_{numCols}')
    helper_funcs.make_directory(hologram_dir)
    helper_funcs.make_directories(numRows, numCols, hologram_dir)

    # Calculate row_pixel_lst, col_pixel_lst
    DMD_rows = dmd.numRows
    DMD_cols = dmd.numCols
    row_pixel_lst, col_pixel_lst = helper_funcs.get_rowAndColumnLists(numRows, numCols, DMD_rows=DMD_rows, DMD_cols=DMD_cols)

    if verbose:
        print(f'row_pixel_lst: {row_pixel_lst}')
        print(f'col_pixel_lst: {col_pixel_lst}')
        print('phase_correct: '+('Yes' if phase_correct else 'No'))
        print('amplitude_correct: '+('Yes' if amplitude_correct else 'No'))

    # phase_correct
    if phase_correct:
        try:
            with open(os.path.join(maps_dir, 'currentPhaseMap.txt'), 'rb') as f:
                phaseMap = pickle.load(f)
            print('Generating holograms WITH phase correction.')
        except:
            print('No phase interpolation available')
            print(os.path.join(maps_dir, 'currentPhaseMap.txt') + ' not found. Not using phase map.')        
            phaseMap = np.zeros((DMD_rows,DMD_cols))
    else:
        phaseMap = np.zeros((DMD_rows,DMD_cols))

    # amplitude_correct
    if amplitude_correct:
        try:
            with open(os.path.join(maps_dir, 'currentAmplitudeMap.txt'), 'rb') as f:
                amplitudeMap = pickle.load(f)
            print('Generating holograms WITH amplitude correction.')
        except:
            print('No amplitude interpolation available')
            print(os.path.join(maps_dir, 'currentAmplitudeMap.txt') + ' not found. Not using amplitude map.')                
            amplitudeMap = np.ones((DMD_rows,DMD_cols))
    else:
        amplitudeMap = np.ones((DMD_rows,DMD_cols))

    # Make holograms
    indexList = []
    for r in np.arange(numRows):
        for c in np.arange(numCols):
            for i in np.arange(numPhases):
                indexList.append((r,c,i,row_pixel_lst[r],col_pixel_lst[c],i*2.*np.pi/numPhases))

    hologram_start = time.time()
    if is_notebook():
        # evaluation in notebook is being weird about parallel
        for x in indexList:
            makeOneHologram(x[0], x[1], x[2], x[3], x[4], x[5],
                        numRows, numCols, numPhases,
                        phaseMap, amplitudeMap,
                        hologram_dir, maps_dir,
                        w, n1, n2, a, d, angle_deg,
                        dmd)
    else:
                Parallel(n_jobs=-1)(delayed(makeOneHologram)(x[0], x[1], x[2], x[3], x[4], x[5],
                                                    numRows, numCols, numPhases,
                                                    phaseMap, amplitudeMap,
                                                    hologram_dir, maps_dir,
                                                    w, n1, n2, a, d, angle_deg,
                                                    dmd) for x in indexList)

    hologram_end = time.time()

    print('Time taken for making holograms: ' + helper_funcs.convertSeconds(hologram_end-hologram_start))


def makeOneHologram(row_index, col_index, phase_index, row_pixel, col_pixel, phaseOffset,
                    numRows, numCols, numPhases,
                    phaseMap, amplitudeMap,
                    hologram_dir, maps_dir,
                    w, n1, n2, a, d, angle_deg,
                    dmd):
    """Creates bmp images used as holograms""" 

    print(f'Making {1 + phase_index + numPhases*col_index + numPhases*numCols*row_index} of {numRows*numCols*numPhases} holograms: r={row_index}, c={col_index}, i={phase_index}')
    # Make a hologram
    xc = dmd.pixelCol_to_xCoord(col_pixel)
    yc = dmd.pixelRow_to_yCoord(row_pixel)

    holomaker = holo_maker.holo_maker(phaseMap, amplitudeMap, dmd, sharpness=a, period=d, angle=angle_deg)
    holomaker.addLG(w,n1,n2,0,0,(0,0),0)
    holomaker.addLG(w,n1,n2,0,0,(xc,yc), phaseOffset)
    hologram = holomaker.makeHologram()

    # Save
    hologram_path = os.path.join(helper_funcs.get_child_directory_path(hologram_dir, row_index, col_index),
                                os.path.basename(helper_funcs.get_child_directory_path(hologram_dir, row_index, col_index)) + '_' + str(phase_index) + '.bmp')  
    Image.fromarray(hologram.astype(np.uint8)).save(hologram_path)


def is_notebook():
    """ see if running in notebook; this is hacky """
    try:
        ipy_name = get_ipython().__class__.__name__
        if ipy_name == 'TerminalInteractiveShell':
            return True
        else:
            return False
    except NameError:
        return False