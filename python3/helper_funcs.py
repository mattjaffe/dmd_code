"""

Helper functions for the rest of the DMD operations

"""

import numpy as np
import os
import shutil
import time
from sys import exit
from PIL import Image

import DMDwrapper
import holo_maker


def get_main_directory_path():
    return os.path.dirname(os.path.abspath(__file__))

def make_directory(directory_path):
    if not os.path.exists(directory_path):
        os.mkdir(directory_path)
    else:
        shutil.rmtree(directory_path)
        os.mkdir(directory_path)

def make_directories(numRows, numCols, directory_path):
    for row_index in np.arange(numRows):
        for col_index in np.arange(numCols):
            os.mkdir(get_child_directory_path(directory_path, row_index, col_index))

def get_child_directory_path(parent_directory_path, row_index, col_index):
    return os.path.join(parent_directory_path, os.path.basename(parent_directory_path) + '_' + str(row_index) + '_' + str(col_index))
        
def get_rowAndColumnLists(numRows, numCols, DMD_rows=684, DMD_cols=608):

    row_pixel_lst = np.arange(numRows) * int(round(DMD_rows/numRows)) + int(round(DMD_rows/(2*numRows)))
    col_pixel_lst = np.arange(numCols) * int(round(DMD_cols/numCols)) + int(round(DMD_cols/(2*numCols)))

    return row_pixel_lst, col_pixel_lst

def convertSeconds(seconds):
    seconds = int(round(seconds))
    minutes = int(seconds / 60)
    if minutes == 0:
        return "%d s" % seconds
    else:
        hours = int(minutes / 60)
        if hours == 0:
            return "%d min, %d s" % (minutes, seconds%60)
        else:
            return "%d hr, %d min, %d s" % (hours, minutes%60, seconds%60)

def display_patches_hologram(numRows, numCols, row_index, col_index, phase_index=0,
                            main_dir=None,holo_dir=None, generate_hologram=False):
    if generate_hologram==False:
        if main_dir is None:
            main_dir = os.path.join(get_main_directory_path(),'calibration')
        if holo_dir is None:
            holo_dir = os.path.join(main_dir, f'DMD_hologram_{numRows}_{numCols}')    
        this_holo_dir = get_child_directory_path(holo_dir, row_index, col_index)
        holo_fname = os.path.join(this_holo_dir, os.path.basename(f'{this_holo_dir}_{phase_index}.bmp'))
        print(f'displaying hologram from {holo_fname}')
        DMDwrapper.display_bitmap(holo_fname,1)
    else:
        print('IMPLEMENT ABILILTY TO GENERATE HOLOGRAMS!!!')


def turn_on_all_mirrors():
    all_on = (np.ones((684,608))*255.9).astype(np.uint8)
    all_on_path = os.path.join(get_main_directory_path(), 'all_mirrors_on.bmp')   
    Image.fromarray(all_on).save(all_on_path)
    DMDwrapper.display_bitmap(all_on_path,1)

def turn_off_all_mirrors():
    all_off = np.zeros((684,608)).astype(np.uint8)
    all_off_path = os.path.join(get_main_directory_path(), 'all_mirrors_off.bmp')   
    Image.fromarray(all_off).save(all_off_path)
    print('trying to send to dmd...')
    DMDwrapper.display_bitmap(all_off_path,1)


def turn_on_full_dmd_supergrating(dmd, a=2, d=4, angle_deg=20., hologramSavePath=None, 
                    phaseMap=None, amplMap=None):
    if hologramSavePath is None:
        saveTo =  os.path.join(get_main_directory_path(), 'test')
        hologramSavePath = os.path.join(saveTo, 'hologram0.bmp')
    if phaseMap is None:
        phaseMap = np.zeros((dmd.numRows,dmd.numCols))
    if amplMap is None:
        amplMap = np.ones((dmd.numRows,dmd.numCols))

    holomaker = holo_maker.holo_maker(phaseMap,amplMap, dmd,
                                      sharpness=a,period=d,angle=angle_deg)
    hologram = holomaker.makeHologram()
    sv_im = Image.fromarray(hologram.astype(np.uint8)).save(hologramSavePath)
    DMDwrapper.display_bitmap(hologramSavePath,1)
