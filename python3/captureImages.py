"""

Capture images

For the code below:
    - A Keithley 3390 triggers a FLIR CMLN-13S2C-CS camera.
    - The Keithley is plugged into the lab network via Ethernet, and software triggered by telnetlib
        - It is manually set to Burst mode, N_cycles = 1, software trigger
        - The output goes to the trigger terminal of the camera
    - The camera is connected to the computer by USB
        - Use the FLIR viewer GUI on the computer
        - Set the camera parameters in the GUI
            - especially 'shutter', to not saturate the images [ruins phase estimation])
            - turn triggering on
            - enable image recording (this option is under 'File' or something in the GUI; not with the camera settings)
                - set to save images to temp_dir as png

Obviously, this procedure could be changed if needed.

"""


# General
import numpy as np
import os
import time
import shutil

# Specific
import helper_funcs
import DMDwrapper
import telnetlib

 # !!! Check these for a new setup !!!
host = "192.168.1.33" # This is the USB address of the Keithley 3390
port = "5024" # This is the port the Keithley 3390 likes to use

#######################################################################

def captureImages(row_pixel_lst, col_pixel_lst,
                    hologram_directory_path, image_directory_path, temp_directory_path, sampleCount):

    numRows = row_pixel_lst.size
    numCols = col_pixel_lst.size

    for row_index in np.arange(numRows):
        for col_index in np.arange(numCols):

            child_hologram_directory_path = helper_funcs.get_child_directory_path(hologram_directory_path, row_index, col_index)
            child_image_directory_path = helper_funcs.get_child_directory_path(image_directory_path, row_index, col_index)
            
            for phase_index in np.arange(3):

                print("Capturing %d of %d images from holograms: r=%d, c=%d, i=%d" % \
                      (1 + phase_index + 3*col_index + 3*numCols*row_index, numRows*numCols*3, row_index, col_index, phase_index))

                # Display hologram on DMD; 
                hologram_path = os.path.join(child_hologram_directory_path, \
                                             os.path.basename(child_hologram_directory_path + "_" + str(phase_index) + ".bmp"))
                DMDwrapper.display_bitmap(hologram_path,1)

                while np.array(os.listdir(temp_directory_path)).size != (1 + phase_index + 3*col_index + 3*numCols*row_index + sampleCount):
                    print(1 + phase_index + 3*col_index + 3*numCols*row_index + sampleCount)

                    #Connect the device
                    tn = telnetlib.Telnet(host, port)
                    tn.read_until(b'3390>')
                    tn.write(b'*IDN?\n')
                    tn.read_until(b'3390>')
                
                    #Software trigger
                    tn.write(b'TRIG\n')
                    tn.read_until(b'3390>')
                    tn.close()

                    # need to wait long enough for the camera software to save the full file; otherwise image gets cropped
                    time.sleep(1)

                    # Rename the files individually (if saved correctly)
                    if np.array(os.listdir(temp_directory_path)).size == (1 + phase_index + 3*col_index + 3*numCols*row_index + sampleCount):
                        image_files = getSortedFiles(temp_directory_path)
                        shutil.copyfile(os.path.join(temp_directory_path, image_files[-1]), \
                                        os.path.join(child_image_directory_path, \
                                                     os.path.basename(child_image_directory_path) + "_" + str(phase_index) + ".png"))

# Sort by time
def getSortedFiles(dir_path):
    cwd = os.getcwd()
    os.chdir(dir_path)
    files = os.listdir(dir_path)
    files.sort(key=os.path.getmtime)
    os.chdir(cwd)
    return np.array(files)
