# using 'latin-1' encoding --> need 256!!!!
# everything below is for the DLP LightCrafter3000; would have to generalize
# or write another version for a new DMD model / Class

"""
    This file is part of DLPLC Interface.

    DLPLC Interface is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DLPLC Interface is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DLPLC Interface.  If not, see <http://www.gnu.org/licenses/>.
"""

from dlplc_tcp import LightCrafterTCP, Packet
import argparse
import sys
import numpy as np

def connect():
    """Connect to a dlplc or exit"""
    dlplc = LightCrafterTCP()
    if not dlplc.connect():
        print('Unable to connect to device.')
        sys.exit(1)
    return dlplc

def get_version():
    dlplc = connect()
    v = dlplc.cmd_version_string(0x00)
    print('DM365 SW Revision: %s'%v.data)
    v = dlplc.cmd_version_string(0x10)
    print('FPGA Firmware Revision: %s'%v.data)
    v = dlplc.cmd_version_string(0x20)
    print('MSP430 SW Revision: %s'%v.data)
    dlplc.close()
    return 0

def display_bitmap(filename,color, verbose=True):
    dlplc = connect()
    v = dlplc.cmd_current_display_mode(0x00)
    dlplc.cmd(Packet.PT_H_WRITE, 0x02, 0x01, "%c%c%c"%(60,1,color)) #60 hz, 1 bit color, monochrome
    print("Current display mode: %s"%":".join(c.encode('hex') for c in v.data))

    with open(filename, mode="rb") as f:
        data = f.read()
    if verbose: print("Loading image...")
    response = dlplc.cmd_static_image(data.decode('latin-1')) # need 256!!!!
    if verbose: print("    Done.")

    try:
        response.raise_if_error()
    except:
        print("Error:")
        response.show()
        return 1

    dlplc.close()
    return 0

def display_data(data):
    dlplc = connect()
    v = dlplc.cmd_current_display_mode(0x00)
    color=1
    dlplc.cmd(Packet.PT_H_WRITE, 0x02, 0x01, "%c%c%c"%(60,1,color)) #60 hz, 1 bit color, monochrome
    print("Current display mode: %s"%":".join(c.encode('hex') for c in v.data))

    print("Loading image...")
    response = dlplc.cmd_static_image(data)
    print("    Done.")

    try:
        response.raise_if_error()
    except:
        print("Error:")
        response.show()
        return 1

    dlplc.close()
    return 0
