"""

DMD class structures

"""
import numpy as np

class DMD():
    def __init__(self, numRows, numCols, pixel_pitch_um, mirr_tilt_deg, aoi_deg, offState=0):
        # properties of the DMD chip
        self.numRows = numRows
        self.numCols = numCols
        self.pixel_pitch_um = pixel_pitch_um
        self.mirr_tilt_deg = mirr_tilt_deg
   
        # properties of the setup
        # depending on which reflection is being taken, off could mean either mirror state
        self.offState = offState
        self.aoi_deg = aoi_deg
        # this is the angle between the outgoing k-vector and the normal to the DMD surface
        # it's relevant for the x-compression of the image
        self.beta_deg = self.aoi_deg - self.mirr_tilt_deg



class LightCrafter3000(DMD):
    def __init__(self, aoi_deg=0, offState=0):
        NUMROWS = 684
        NUMCOLS = 608
        PX_PITCH_UM = 7.637
        MIRROR_TILT_ANGLE_DEG = 12
        super().__init__(NUMROWS, NUMCOLS, PX_PITCH_UM, MIRROR_TILT_ANGLE_DEG,aoi_deg,offState)

        self.evm_datasheet_link = 'https://www.ti.com/lit/dlpu006'
        self.command_interface_guide_link = 'https://www.ti.com/lit/ug/dlpu007d/dlpu007d.pdf'
        self.DMD_datasheet_link = 'https://media.digikey.com/pdf/Data%20Sheets/Texas%20Instruments%20PDFs/DLP3000.pdf'

    def pixelCol_to_xCoord(self, colnum):
        """ pixel coordinates --> transverse plane x-coordinate """
        # transverse plane coordinate center = center of DMD
        # when viewed from the transverse plane of the outgoing beam, the image displayed
        # on the DMD plane is compressed in the x-direction
        return (colnum -self.numCols/2)*np.cos(self.beta_deg*np.pi/180)

    def pixelRow_to_yCoord(self, rownum):
        """ pixel coordinates --> transverse plane y-coordinate """
        # transverse plane coordinate center = center of DMD
        # accounts for row squeeze (adjacent rows overlap by 1/2 on physical device; see datasheet)
        return (rownum - self.numRows/2)*0.5
