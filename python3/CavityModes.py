"""
classes and functions for creating holograms of typical cavity modes

originally written by Ariel Sommer
"""

import numpy as np
from scipy.integrate import dblquad

pi = np.pi
Inf = np.inf

class AbstractMode(object):
    def __init__(self,offset=(0,0)):
        if offset:
            self.offset = offset
        else:
            self.offset = (0,0)
        
    def evalPoly(self,coefs,x):
        f=0
        for m in range(len(coefs)):
            f += coefs[m] * x**m
        return f
    
    def findOverlap(self,mode):            
        f = lambda x,y: self.modeProfWithOffset([x,y])*np.conj(mode.modeProfWithOffset([x,y]))
        fr = lambda x,y: np.real(f(x,y))
        fi = lambda x,y: np.imag(f(x,y))
        lim1 = lambda x: -Inf
        lim2 = lambda x: Inf
        epsabs = 1e-2; epsrel=1e-2
        ir, _ = dblquad(fr,-Inf,Inf,lim1,lim2,(),epsabs,epsrel)
        ii, _ = dblquad(fi,-Inf,Inf,lim1,lim2,(),epsabs,epsrel)
        return ir + 1j*ii

    def modeProfWithOffset(self,pos):
        shifted = [pos[0]-self.offset[0], pos[1]-self.offset[1]]
        return self.modeProfile(shifted)
        
    def modeProfile(self,pos):
        print("Abstract base class method modeProfile implemented")
        return None
    
    def getDirection(self):
        return self.direction

class HGMode(AbstractMode):
    def __init__(self,waistX,waistY,nx,ny, phi, epsilon_phi,offset=(0,0)):
        super(HGMode,self).__init__(offset)
        self.waistX=waistX
        self.waistY=waistY
        self.hcoefsX = []
        self.hcoefsY = []
        for k in range(nx+1):
            self.hcoefsX.append(self.hcoef(nx,k))
        for k in range(ny+1):
            self.hcoefsY.append(self.hcoef(ny,k))
        self.preFactor =np.sqrt(2/(pi*waistX*waistY*2**(nx+ny)*np.math.factorial(nx)*np.math.factorial(ny)))
        self.phi = phi
        self.epsilon_phi = epsilon_phi
        
    def hcoef(self,n,k):
        #Recursively finds the k-th coefficient of H_n
        if (n%2 != k%2 or k > n):
            return 0
        if (n==0 and k==0):
            return 1
        elif (n==1 and k==1):
            return 2
        elif (k>0):
            return 2*self.hcoef(n-1,k-1)-2*(n-1)*self.hcoef(n-2,k)
        elif (k==0):
            return -2*(n-1)*self.hcoef(n-2,0)
        print("Error in hcoef: how did I get here?")

    def modeProfile(self,pos):
        x = np.sqrt(2)/self.waistX * np.sqrt(1. - 1.j*self.phi*(1+self.epsilon_phi)) * pos[0] #dimensionless coordinates
        y = np.sqrt(2)/self.waistY * np.sqrt(1. - 1.j*self.phi*(1-self.epsilon_phi)) * pos[1]
        R2 = x**2 + y**2
        hx = self.evalPoly(self.hcoefsX,x)
        hy = self.evalPoly(self.hcoefsY,y)
        return self.preFactor*np.exp(-0.5*R2)*hx*hy

class LGMode(AbstractMode):
    """
    Represents a Laguerre-Gauss mode
    p - polynomial order
    l - angular momentum
    """
    def __init__(self,waist,p,l, phi, epsilon_phi, offset=(0,0),defocus=0, tilt=(0,0), intensity=1):
        super(LGMode,self).__init__(offset)
        self.waist = waist
        self.p=p
        self.l=l
        self.coefs = [] #Laguerre polynomial coefficients
        self.intensity = intensity
        for m in range(p+1):
            self.coefs.append(self.LGcoef(l,p,m))
        k=abs(l)
        self.preFactor = 1.0/waist*np.sqrt(2/pi*np.math.factorial(p)/np.math.factorial(p+k))
        if defocus:
          self.defocFac = 1.j*np.pi*defocus
        else:
          self.defocFac = 0
        self.phi = phi
        self.epsilon_phi = epsilon_phi
        # I have no idea what this transformation is called.
        # It does some sort of rotating/stretching of the x-y plane.
        # But I don't have a clue as to the parametrization.
        self.tilt=tilt
        
        
    def LGcoef(self,l,p,m):
        """
        Returns the coefficient of the m-th orer term of the Assoc. Laguerre 
        polynomial of upper index abs(l) and order p
        """
        k=abs(l)
        num = (-1)**m * np.math.factorial(p+k)
        den = np.math.factorial(p-m)*np.math.factorial(k+m)*np.math.factorial(m)
        return num/den
        
    def evalLaguerre(self,x):
        f = 0
        for m in range(len(self.coefs)):
            f += self.coefs[m] * x**m
        return f
        
    def modeProfile(self,pos):
        x = np.sqrt(2)/self.waist * np.sqrt(1. - 1.j*self.phi*(1+self.epsilon_phi)) * pos[0] #dimensionless coordinates
        y = np.sqrt(2)/self.waist * np.sqrt(1. - 1.j*self.phi*(1-self.epsilon_phi)) * pos[1]
        z = x + 1j*y
        R2 = abs(z)**2
        # I'm pretty sure this part is only for doing offset modes!!! Therefore, this section *assumes* that the 00 mode is
        # exactly centered on the DMD! Need to add another centering parameter 
        xd = np.sqrt(2)/self.waist * np.sqrt(1. - 1.j*self.phi*(1+self.epsilon_phi)) * (pos[0] + 0*self.offset[0]) #dimensionless coordinates
        yd = np.sqrt(2)/self.waist * np.sqrt(1. - 1.j*self.phi*(1-self.epsilon_phi)) * (pos[1] + 0*self.offset[1])
        R2def = xd**2 + yd**2 # radius for defocussing around (why different than other???)
        zl = z**abs(self.l)
        if (self.l<0):
            zl = np.conj(zl)
        laguerre = self.evalPoly(self.coefs,R2)#self.evalLaguerre(R2)
        return self.intensity * self.preFactor * zl * np.exp(-0.5*R2) * laguerre * np.exp(self.defocFac*R2def) * np.exp(1.j*(x*self.tilt[0] + y*self.tilt[1]))
     
