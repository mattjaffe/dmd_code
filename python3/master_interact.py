import os
from PIL import Image
import numpy as np
import time
import pickle

import helper_funcs
import holo_maker
import DMDwrapper
import dmds

main_dir = helper_funcs.get_main_directory_path()
maps_dir = os.path.join(main_dir, "maps")

def initHolomaker(a=2, d=8, angle_deg=35, verbose=False):

    MAXNUMROWS = 684
    MAXNUMCOLS = 608
    main_dir = helper_funcs.get_main_directory_path()
    maps_dir = os.path.join(main_dir, "maps")

    # phaseMap
    phaseMap_interpolated = np.zeros((MAXNUMROWS,MAXNUMCOLS))+0.0
    usePhaseMap = True
    if usePhaseMap:
        with open(os.path.join(maps_dir, 'currentPhaseMap.txt'), 'rb') as f:
            phaseMap_interpolated = pickle.load(f)
            phaseMap_interpolated = phaseMap_interpolated

    # amplitudeMap
    amplitudeMap_interpolated = np.zeros((MAXNUMROWS,MAXNUMCOLS))+1.0
    useAmplMap = False
    if useAmplMap:
        with open(os.path.join(maps_dir, 'currentAmplMap.txt'), 'rb') as f:
            amplitudeMap_interpolated = pickle.load(f)


    #create blank
    dmd = dmds.LightCrafter3000(aoi_deg=45)
    if verbose:
        print('Initializating holomaker')
    holomaker = holo_maker.holo_maker(phaseMap_interpolated,amplitudeMap_interpolated, dmd,
                                      sharpness=a,period=d,angle=angle_deg)
    return holomaker

def saveHologram(hologram, save=0):
    saveTo =  os.path.join(main_dir, 'test')
    hologram_path = os.path.join(saveTo, f'hologram{save}.bmp')  
    sv_im = Image.fromarray(hologram.astype(np.uint8)).save(hologram_path)
    return hologram_path

def makeHG(waistX, waistY, nx, ny, phi, epsilon_phi, center, verbose=False, save=0, **holo_kwargs):
    holomaker = initHolomaker(**holo_kwargs,verbose=verbose)
    #add HG
    if (verbose):print('Adding HG')
    holomaker.addHG(waistX, waistY, nx, ny, phi, epsilon_phi, center, 0)
    if (verbose):print('making hologram')
    hologram = holomaker.makeHologram()
    #save
    hologram_path=saveHologram(hologram, save)
    #display on DMD
    DMDwrapper.display_bitmap(hologram_path,1)
    return hologram_path

def makeLG(waists, n1s, n2s, centers, verbose=False,save=0,defocuses=[0],tilts=[(0.0,0.0)], phaseOffs=[0], 
            phis=[0], eps_phis=[0], display=False, **holo_kwargs):
    holomaker = initHolomaker(**holo_kwargs,verbose=verbose)
    #add LG
    for i in range(len(waists)):
        holomaker.addLG(waists[i], n1s[i], n2s[i], phis[i], eps_phis[i], centers[i],phaseOffset=phaseOffs[i],defoc=defocuses[i], tlt=tilts[i])
    hologram = holomaker.makeHologram()
    #save
    hologram_path=saveHologram(hologram, save)
    #display on DMD
    if display:
        DMDwrapper.display_bitmap(hologram_path,1)
    return hologram_path

def makeLGs(n1s, n2s, waist, center,intensities=None,
            defocus=0,tilt=(0.0,0.0),phi=0,epsilon_phi=0,phaseOffs=0,
            verbose=False,save=0, display=False, **holo_kwargs):
    holomaker = initHolomaker(**holo_kwargs,verbose=verbose)
    #add LG
    l1s, l2s = (n1s, np.zeros(len(n1s))+n2s[0]) if (len(n1s)>len(n2s) or (sum(n2s)==0)) else  (np.zeros(len(n2s))+n1s[0], n2s)
    for i in range(len(l1s)):
        holomaker.addLG(waist, int(l1s[i]), int(l2s[i]), phi, epsilon_phi, center, 
                        phaseOffset=(0 if i==0 else phaseOffs),defoc=defocus, tlt=tilt,
                        intensity=(1 if intensities is None else intensities[i]))
    hologram = holomaker.makeHologram()
    #save
    hologram_path=saveHologram(hologram, save)
    #display on DMD
    if display:
        DMDwrapper.display_bitmap(hologram_path,1)
    return hologram_path

def HGLG_ladder(w=50, lmax=6, cc=(-25,20), already_calculated=False, holo_dir=None, delay=None):
    holograms_path = []
    # generate holograms or their file locations:
    for ii in np.arange(2*lmax + 1):
        if not already_calculated:
            print(f'pre-calculating hologram {ii+1} of {2*lmax + 1}...')
            if ii>lmax:
                holograms_path.append(makeHG(w,w, (ii-lmax),(ii-lmax) ,0,0, cc, 4, verbose=False,save=ii))
            else:
                holograms_path.append(makeLG([w], [(ii-lmax) if ii>lmax else 0],[-(ii-lmax) if ii<lmax else 0], [cc],
                                        wp=4,verbose=False,save=ii))
        else:
            if holo_dir is None:
                main_dir = helper_funcs.get_main_directory_path()
                holo_dir = os.path.join(main_dir, 'test')
            holograms_path.append(os.path.join(holo_dir, f'hologram{ii}.bmp'))

    # display holograms
    cnt=0
    while(-cnt<2*lmax+2):
        holopath = holograms_path[(cnt-(lmax+1))%(2*lmax+1)]
        DMDwrapper.display_bitmap(holopath,1)
        if delay is not None:
            time.sleep(delay)
        cnt-=1

    return holograms_path

def LG0l_interf(w=50, lmax=12, already_calculated=False, cc=(0,35), holo_dir=None, delay=None):
    holograms_path = []
    # generate holograms or their file locations:
    for ii in np.arange(lmax):
        if not already_calculated:
            print(f'pre-calculating hologram {ii+1} of {lmax}...')
            holograms_pth.append(makeLG([w, w], [0, ii],[0, 0], [cc,cc], wp=4,verbose=False,save=ii,
                                tilts=[(0,0),(0,0)], defocuses=[0, 0], phaseOffs=[0,0]))
        else:
            if holo_dir is None:
                main_dir = helper_funcs.get_main_directory_path()
                holo_dir = os.path.join(main_dir, 'test')
            holograms_path.append(os.path.join(holo_dir, f'hologram{ii}.bmp'))
    # display holograms
    for ii in np.arange(lmax):
        DMDwrapper.display_bitmap(holograms_path[ii],1)
    return holograms_path

def spinning_spots(w=50, numPhases=3, l1=0,l2=1, already_calculated=False, cc=(0,35), df=0, holo_dir=None, delay=None):
    holograms_path = []
    # generate holograms or their file locations:
    for ii in np.arange(numPhases):
        if not already_calculated:
            print(f'pre-calculating hologram {ii+1} of {numPhases}...')
            holograms_path.append(makeLGs([l1, l2], [0],w, cc,tilt=tt, defocus=df, phaseOffs=2*np.pi/numPhases*ii,
                            verbose=False, save=ii, display=True, **holo_grating_kwargs))
        else:
            if holo_dir is None:
                main_dir = helper_funcs.get_main_directory_path()
                holo_dir = os.path.join(main_dir, 'test')
            holograms_path.append(os.path.join(holo_dir, f'hologram{ii}.bmp'))
    # display holograms
    for ii in np.arange(numPhases):
        DMDwrapper.display_bitmap(holograms_path[ii],1)
    DMDwrapper.display_bitmap(holograms_path[0],1)
    return holograms_path

def LG_ladder(  lmax=12, lmin=0, dL=1, idx=0,
                w=50, cc=(0,0), tt=(0,0), defoc=0, phi=0, epsilon_phi=0,
                already_calculated=False, holo_dir=None, delay=None, **holo_kwargs):
    
    holograms_path = []
    # generate holograms or their file locations:
    ells = np.arange(lmin,lmax+1)
    l1s, l2s = (ells, np.zeros(len(ells))) if (idx==0) else  (np.zeros(len(ells)), ells)

    for ii, ell in enumerate(ells):
        if not already_calculated:
            print(f'pre-calculating hologram {ii+1} of {len(ells)}: l = {ell}...')
            holograms_path.append(
                                    makeLG( [w], [int(l1s[ii])],[ int(l2s[ii])],
                                            [cc], tilts=[tt], defocuses=[defoc], phaseOffs=[0], phis=[phi], eps_phis=[epsilon_phi],
                                            verbose=False, save=ii, **holo_kwargs
                                            )
                                    )

        else:
            if holo_dir is None:
                main_dir = helper_funcs.get_main_directory_path()
                holo_dir = os.path.join(main_dir, 'test')
            holograms_path.append(os.path.join(holo_dir, f'hologram{ii}.bmp'))
    # display holograms
    for ii in np.arange(len(ells)):
        if ii%dL==0:
            DMDwrapper.display_bitmap(holograms_path[ii],1)
            if delay is not None:
                time.sleep(delay)
    return holograms_path

def norm_fac(p, l, waist):
    L = np.abs(l)
    mode_function_prefactor = 1/waist*np.sqrt(2/np.pi*np.math.factorial(p)/np.math.factorial(p+L))
    unnorm_LL_fac = 1/((L**(L/2))*np.exp(-L/2))
    return unnorm_LL_fac / mode_function_prefactor

if __name__ == "__main__":
    holo_grating_kwargs = {'a':2, 'd':4, 'angle_deg':125.}
    # n1 = 0
    # n2 = 0
    ppp = 0
    nR = ppp
    nL = ppp
    cc = (-2, 0)
    tt = (0., 0.0)

    w = 58
    df = -0.35
    phi = 0.0
    epsilon_phi = 0.00
    xx = 0
    # hologram_path1 = makeLG([w], [nR], [nL], [cc], verbose=True, save=32 ,defocuses=[df],tilts=[tt],
    #         phis=[phi], eps_phis=[epsilon_phi], display=True, **holo_grating_kwargs)
    ss = 3
    lls = [2]
    intenss = [norm_fac(0, ll, w) for ll in lls]

    hologram_path2 = makeLGs(lls if ss==1 else [xx], [xx] if ss==1 else lls,
                             w, cc, tilt=tt, defocus=df, phi=phi, epsilon_phi=epsilon_phi, intensities=intenss,
                             verbose=False, save=31, display=True, **holo_grating_kwargs)















    # hologram_path1 = makeHG(w,w, n1,n2 ,0,0, cc, verbose=True,save=1, **holo_grating_kwargs)
    
    # LG_ladder(lmax=30, lmin=5, dL=5, idx= (0 if ss==1 else 1), w=w, cc=cc, tt=tt, defoc=df, phi=phi, epsilon_phi=epsilon_phi,
    #             already_calculated=True, holo_dir=None, delay=None, **holo_grating_kwargs)

    # HGLG_ladder(w=w, lmax=9,False already_calculated=already_calculated, cc=cc)

 # while (True):
    #     DMDwrapper.display_bitmap(hologram_path1,1)
    #     time.sleep(1)
    #     DMDwrapper.display_bitmap(hologram_path2,1)
    #     time.sleep(1)