Issues
------

Changes made that need DMD verification
---------------------------------------

To-do
-----
- Change LG scaling so that can show multiple LG modes on at the same time!!
- add defocus / tilt to HG modes
- restructure / rename master_interact
- add roi picker/fitter for images
- add legends to row plots
- add option to Bool instead of LG? and/or scale patch LG such that don't need to manually specify waist?
- add GUI/plot that's showing what it's doing? i.e., plot the hologram it just made, show the image it just took, etc.