"""

Class for generating DMD holograms

"""


import numpy as np

import CavityModes as CM


pi = np.pi

class holo_maker:
    def __init__(self,phaseMap,amplitudeMap,dmd,sharpness,period,angle):
        self.a = sharpness
        self.d = period
        self.angle_deg = angle
        
        self.DMD_rows=dmd.numRows
        self.DMD_cols=dmd.numCols
        self.DMD_shape=(self.DMD_rows,self.DMD_cols)
        
        xs = dmd.pixelCol_to_xCoord(np.arange(self.DMD_cols))
        ys = dmd.pixelRow_to_yCoord(np.arange(self.DMD_rows))

        self.X, self.Y = np.meshgrid(xs,ys)
        
        self.mirrorOn = 0
        self.mirrorOff = 255

        self.mirrorOff = 255*dmd.offState
        self.mirrorOn = 255-self.mirrorOff

        self.phaseMap = phaseMap
        self.amplitudeMap = amplitudeMap
        self.resetTargetFunction()
    
    def resetTargetFunction(self):
      self.targetFunction=np.zeros(self.DMD_shape,np.complex)

    def addMode(self,mode,phaseOffset):
      self.targetFunction += mode.modeProfWithOffset([self.X,self.Y]) * np.exp(1j*phaseOffset)
  
    def addHG(self,waistX,waistY,nx,ny, phi, epsilon_phi, center=(0,0), phaseOffset=0):
      mode = CM.HGMode(waistX,waistY,nx,ny, phi, epsilon_phi, offset=center)
      self.addMode(mode,phaseOffset)

    def addLG(self,waist,n1,n2, phi, epsilon_phi, center=(0,0),phaseOffset=0,defoc=0,tlt=(0.0,0.0),intensity=1):
      p = min([n1,n2])
      l = n1-n2
      mode = CM.LGMode(waist,p,l, phi, epsilon_phi, offset=center, defocus=defoc, tilt=tlt, intensity=intensity)
      self.addMode(mode,phaseOffset)

    def makeHologram(self): 
        targetFunction=self.targetFunction
        targetMag = np.abs(targetFunction)
        targetPhase = np.angle(targetFunction)  # \in (-pi,pi]
        targetPhase +=  self.phaseMap # watch out for the sign (change as needed)! <--?
        mag = targetMag * self.amplitudeMap
        
        mag -= np.min(mag)
        maxV = np.max(mag)
        if maxV != 0:
            mag /= maxV
        else:
            mag += 1
        # mag is now in range [0,1]

        angle_rad = self.angle_deg * pi/180.0
        kx = np.cos(angle_rad)*2*pi/self.d  
        ky = np.sin(angle_rad)*2*pi/self.d
        phase = np.mod(kx*self.X + ky*self.Y + targetPhase,2*pi)
        
        for i in range(self.DMD_rows):
            for j in range(self.DMD_cols):
                if (phase[i,j] > pi):
                    phase[i,j] -= 2*pi
        # phase is now in range (-pi,pi]

        #This might need some fiddling if the on/off boolean is reversed; i.e., if you take the "off"-state DMD reflection
        prob =0.5*(np.tanh(self.a*(phase+pi*mag/2))+np.tanh(self.a*(pi*mag/2-phase)))
        bimage = np.zeros((self.DMD_rows,self.DMD_cols),dtype=np.uint8)
        r = np.random.rand(self.DMD_rows,self.DMD_cols) 
        if self.mirrorOff == 0:
          bimage = 1 - ((r>prob).astype(int))
        else:
          bimage = 1 - ((r<prob).astype(int))
        bimage *= max(self.mirrorOn, self.mirrorOff)
        
        return bimage