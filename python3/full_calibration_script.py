"""

Script for calibrating DMD

    First get the phase map: 
        - generate holograms
        - capture images using those holograms
        - fit images to extract phase map

    Next, use that phase map to get the amplitude map. Using phase map from initial
    calibration, re-do procedure to back out amplitude map. Unclear how much of an
    effect this has; the phase map is way more important. In this iteration,
    the resulting fitted phase map should be flat

"""

# To-do
# - add DMD AOI parameter as hologram kwarg

import numpy as np

from z_make_the_holograms import make_the_holograms
from z_take_the_images import take_the_images
from z_calc_the_phases import calc_the_phases
import dmds


###############################################################################################
# DMD & setup
dmd = dmds.LightCrafter3000(aoi_deg=45)
#number of patches to segment the DMD into(see Zupancic thesis Fig. 3.2)
numRows = 10
numCols = 10

# First pass: phase map
make_holograms_for_phase_map = False
capture_images_for_phase_map = True
calc_phases_from_images_for_phase_map = True
show_map_plots = [True, True, True, True] # [phase_discrete, phase_interpolated, ampl_discrete, ampl_interpolated]
overwrite_maps =  [True, False] # [phase_map, ampl_map]

# Second pass: amplitude map
make_holograms_for_ampl_map_using_phase_map = False
capture_images_for_ampl_map = False
calc_phases_from_images_for_ampl_map = False
show_map_plots = [True, True, True, True] # [phase_discrete, phase_interpolated, ampl_discrete, ampl_interpolated]
overwrite_maps_pass2 = [False, True] # [phase_map, ampl_map]
###############################################################################################

holo_grating_kwargs = {'w':10, 'a':2, 'd':4, 'angle_deg':125.}

roi_img = np.array([0, 1280, 0, 960])
img_center = [605, 417] # eventually fit this from an image near where sampling patch ~ reference patch --> no interference
pinhole_pixel_diameter = 2
roi_pinhole = np.array([img_center[0], img_center[0] + pinhole_pixel_diameter, img_center[1], img_center[1] + pinhole_pixel_diameter])

# First pass
###############################################################################################
# holograms
verbose_holograms = True
if make_holograms_for_phase_map:
    holograms_for_phase_map_kwargs = {'phase_correct':False, 'amplitude_correct':False,'verbose':verbose_holograms}
    make_the_holograms(numRows, numCols, dmd, **holograms_for_phase_map_kwargs, **holo_grating_kwargs)

# capture images
verbose_capture_imgs = True
if capture_images_for_phase_map:
    capture_image_kwargs = {'verbose':verbose_capture_imgs}
    take_the_images(numRows, numCols, dmd, **capture_image_kwargs)

# calculate phases
verbose_maps = True
if calc_phases_from_images_for_phase_map:
    calc_phase_kwargs = {'verbose':verbose_maps, 'show_plots':show_map_plots, 'overwrite_main_maps':overwrite_maps,
                        'row_excl':[], 'col_excl':[] if numCols<10 else [-1]}
    phase_map = calc_the_phases(numRows, numCols, dmd, roi_img=roi_img, roi_pinhole=roi_pinhole, **calc_phase_kwargs)

# Second pass
###############################################################################################
pass2_str = 'pass2_'
# holograms
verbose_holograms = True
if make_holograms_for_ampl_map_using_phase_map:
    holograms_for_ampl_map_kwargs = {'phase_correct':True, 'amplitude_correct':False,'verbose':verbose_holograms, 
                                    'holo_dir_mod':pass2_str}
    make_the_holograms(numRows, numCols, dmd, **holograms_for_ampl_map_kwargs, **holo_grating_kwargs)

# capture images
verbose_capture_imgs = True
if capture_images_for_ampl_map:
    capture_image_kwargs = {'verbose':verbose_capture_imgs, 'holo_dir_mod':pass2_str, 'img_dir_mod':pass2_str}
    take_the_images(numRows, numCols, dmd, **capture_image_kwargs)

# calculate phases
verbose_maps = True
if calc_phases_from_images_for_ampl_map:
    calc_ampl_kwargs = {'verbose':verbose_maps, 'show_plots':show_map_plots, 'overwrite_main_maps':overwrite_maps_pass2,
                        'row_excl':[], 'col_excl':[] if numCols<10 else [-1], 'img_dir_mod':pass2_str}
    phase_map = calc_the_phases(numRows, numCols, dmd, roi_img=roi_img, roi_pinhole=roi_pinhole,**calc_ampl_kwargs)

print('Done!!!')