import os
import pickle
from scipy import interpolate
import numpy as np
import matplotlib.pyplot as plt

import DMDwrapper
import dmds
import helper_funcs

numRows = 10
numCols = 10
row_index = 0
col_index = 0
dmd = dmds.LightCrafter3000(aoi_deg=45)
holo_grating_kwargs = {'a':2, 'd':4, 'angle_deg':125.}

turnOn = True
# turnOn = False

# if turnOn:
#     helper_funcs.turn_on_all_mirrors()
# else:
#     helper_funcs.turn_off_all_mirrors()



regenerate_patch_hologram = False
helper_funcs.display_patches_hologram(numRows, numCols, row_index, col_index, generate_hologram=regenerate_patch_hologram)
# Doesn't make the hologram; add option to do so


# helper_funcs.turn_on_full_dmd_supergrating(dmd, **holo_grating_kwargs)