"""

Take the images for phase mapping / calibration of the DMD
(see Chapter 3 of Philip Zupancic's Master's thesis)

- Divide the DMD into patches and interfere them with each other to find their relative phase

"""

import os
import pickle
import time

import helper_funcs
import captureImages

def take_the_images(numRows, numCols, dmd,
                        verbose=True, main_dir=None, temp_dir=None, hologram_dir=None, img_dir=None,
                        holo_dir_mod='', img_dir_mod=''):

    # Directories
    if main_dir is None:
        main_dir = os.path.join(helper_funcs.get_main_directory_path(),'calibration')
    if temp_dir is None:
        temp_dir = os.path.join(main_dir, 'DMD_temp')
    if hologram_dir is None:
        hologram_dir = os.path.join(main_dir, f'{holo_dir_mod}DMD_hologram_{numRows}_{numCols}')
    if img_dir is None:
        img_dir = os.path.join(main_dir, f'{img_dir_mod}DMD_image_{numRows}_{numCols}')
    helper_funcs.make_directory(temp_dir)
    helper_funcs.make_directory(img_dir)
    helper_funcs.make_directories(numRows, numCols, img_dir)

    # Calculate row_pixel_lst, col_pixel_lst
    row_pixel_lst, col_pixel_lst = helper_funcs.get_rowAndColumnLists(numRows, numCols, DMD_rows=dmd.numRows, DMD_cols=dmd.numCols)

    if verbose:
        print(f'row_pixel_lst: {row_pixel_lst}')
        print(f'col_pixel_lst: {col_pixel_lst}')

    # Take images
    image_start = time.time()
    captureImages.captureImages(row_pixel_lst, col_pixel_lst, hologram_dir, img_dir,temp_dir, sampleCount=0)
    image_end = time.time()

    # Print
    print(f'\n Total time taken: {helper_funcs.convertSeconds(image_end-image_start)} \n')