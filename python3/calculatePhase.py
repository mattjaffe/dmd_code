"""

Calculate phase map from measured patch-interference images

"""

import os
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from scipy.ndimage.filters import gaussian_filter
from scipy import interpolate
from skimage.restoration import unwrap_phase

import helper_funcs

def getCrop(img, roi):
    """ returns img cropped to roi """
    # roi = np.array([xmin, xmax, ymin, ymax]), but image is (row,column)-indexed
    return img[roi[2]:roi[3],roi[0]:roi[1]]

def GaussFilter(amplitudeMap, sigma=3):
    amplitudeMap_filtered = gaussian_filter(amplitudeMap,sigma)
    amplitudeMap_filtered /= np.min(amplitudeMap_filtered)
    return amplitudeMap_filtered

def fixAmplNearRefPatch(amplitudeMap):
    # There's definitely a more general / better way to do this
    [cols, rows] = amplitudeMap.shape
    for tt in np.arange(1):
        for jj in [int(cols/2)]:#, int(cols/2)-1]:
            for ii in [int(rows/2)]:#, int(rows/2)-1]:
                amplitudeMap[jj, ii] = np.mean([amplitudeMap[r, c] for r in np.arange(ii-1,ii+2,1) for c in np.arange(jj-1,jj+2,1)])
    return amplitudeMap

def calculatePhase(row_pixel_lst, col_pixel_lst, img_roi, pinhole_roi, image_directory_path, fit_directory_path, verbose=True):
    """ calculate the phase and intensity maps from the images (finally) """
    numRows = row_pixel_lst.size
    numCols = col_pixel_lst.size
    numPhases = 3
    phaseMap = np.zeros((numRows, numCols))
    amplitudeMap = np.zeros((numRows, numCols))

    saturated = 0
    for row_index in np.arange(numRows):
        for col_index in np.arange(numCols):
            child_image_directory_path = helper_funcs.get_child_directory_path(image_directory_path, row_index, col_index)
            intensity = np.zeros(numPhases)
            
            for phase_index in np.arange(numPhases):
                if verbose: print(f'Fitting {1 + phase_index + numPhases*col_index + numPhases*numCols*row_index} of {numRows*numCols*numPhases} images: r={row_index}, c={col_index}, i={phase_index}')
                image_path = os.path.join(child_image_directory_path,os.path.basename(child_image_directory_path) + f'_{phase_index}.png')
                img = np.asarray(Image.open(image_path))
                region = getCrop(getCrop(img,img_roi),pinhole_roi)
                if (region==255).any(): saturated += 1
                intensity[phase_index] = np.sum(region)

            # Fit
            phasor = -1./3.*(intensity[1]+intensity[2]-2*intensity[0]) + (1.j/np.sqrt(3)) * (intensity[1]-intensity[2])
            phaseMap[row_index,col_index] = np.angle(phasor)
            amplitudeMap[row_index,col_index] = np.abs(phasor)

    if saturated > 0:
        print(f'*** WARNING!!! ***: {saturated} images were saturated! Lower the illumination or your fits will be bad!!')

    # if both numRows and numCols are odd, there will be an image set where target patch = reference patch
    if numRows%2==1 and numCols%2==1:
        phaseMap[int(numRows/2), int(numCols/2)] = 0.0000001
        intensity = np.zeros(numPhases)
        for phase_index in np.arange(numPhases):
            child_image_directory_path = helper_funcs.get_child_directory_path(image_directory_path, int(numRows/2), int(numCols/2))
            image_path = os.path.join(child_image_directory_path,os.path.basename(child_image_directory_path) + f'_{phase_index}.png')
            img = np.asarray(Image.open(image_path))
            region = getCrop(getCrop(img,img_roi),pinhole_roi)
            intensity[phase_index] = np.sum(region)
        amplitudeMap[int(numRows/2), int(numCols/2)] = np.mean(intensity)


    plotAndSaveDiscreteMap(phaseMap, numRows, numCols, fit_directory_path, 'rawPhaseMap')
    plotAndSaveDiscreteMap(amplitudeMap, numRows, numCols, fit_directory_path, 'rawAmplitudeMap')
    return phaseMap, amplitudeMap

def plotAndSaveDiscreteMap(map, numRows, numCols, fit_directory_path, title, verbose=False):
    """ plot and save phase map """
    plt.imshow(map, interpolation='nearest')
    plt.colorbar()
    plt.suptitle(title)
    fname = os.path.join(fit_directory_path, title + '.png')
    if verbose: print('saving to ' + fname)
    plt.savefig(fname, dpi=200)
    plt.close()

def interpolateDiscreteMapAndSavePlt(row_pixel_lst, col_pixel_lst, DMD_rows, DMD_cols, phaseMap, fit_directory_path, title):
    x = col_pixel_lst
    y = row_pixel_lst
    z = phaseMap
    f = interpolate.interp2d(x,y,z,kind='cubic')
    xnew = np.arange(DMD_cols)
    ynew = np.arange(DMD_rows)
    znew = f(xnew,ynew)
    plt.imshow(znew,interpolation='nearest')
    plt.colorbar()
    plt.savefig(os.path.join(fit_directory_path, title + ".png"))
    plt.close()
    return znew

def phaseUnwrap2D(population_row_pixel_lst, population_col_pixel_lst, DMD_rows, DMD_cols,
                    population_phaseMap, fit_directory_path, save_row_plots=False):
    unwrapped_phaseMap = unwrap_phase(population_phaseMap)
    if save_row_plots:
        #plot
        mrkrsz=15
        lw=2
        numCols = population_phaseMap.shape[0]
        for r in np.arange(len(population_row_pixel_lst)):
            # plot raw phases        
            plt.plot(population_col_pixel_lst, population_phaseMap[r,:],'.', c='red',ms=mrkrsz)
            for i in np.arange(-1,2):
                b = population_phaseMap[r,:]+i*2*np.pi
                # plot raw phases + n*2*pi
                plt.plot(population_col_pixel_lst, b,'.',c='gray',ms=mrkrsz, alpha = 0.5)
            # wrapped-phase boundary bars
            plt.plot([0, DMD_cols],[0,0],'k--', lw=lw/2)
            plt.plot([0, DMD_cols],[np.pi,np.pi],'k--',lw=lw/2)
            plt.plot([0, DMD_cols],[-np.pi,-np.pi],'k--',lw=lw/2)
            # plot unwrapped phases        
            plt.plot(population_col_pixel_lst,unwrapped_phaseMap[r,:],c='blue',lw=lw)
            title = f'row {r}'
            plt.suptitle(title)
            plt.xlabel('pixel')
            plt.ylabel('phase [rad]')
            plt.grid(False)
            plt.savefig(os.path.join(fit_directory_path, title + ".png"))
            plt.close()
    
    return unwrapped_phaseMap