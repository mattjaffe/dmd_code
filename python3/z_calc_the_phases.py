"""
Wrapper function for calculating/saving/displaying phase and amplitude maps from measured patch-interference images
"""

import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
import shutil

import helper_funcs
import calculatePhase

def calc_the_phases(numRows, numCols, dmd, row_excl=[], col_excl=[],
    roi_img=np.array([0, 1280, 0, 960]), roi_pinhole=np.array([639, 641, 479, 481]), 
    verbose=True, show_plots=[True, True, True, True], overwrite_main_maps=[False, False], numPhases=3,
    main_dir=None, img_dir=None, fit_dir=None, img_dir_mod=''):

    # Directory paths
    if main_dir is None:
        main_dir = os.path.join(helper_funcs.get_main_directory_path(),'calibration')
    if img_dir is None:
        img_dir = os.path.join(main_dir, f'{img_dir_mod}DMD_image_{numRows}_{numCols}')
    if fit_dir is None:
        fit_dir = os.path.join(main_dir, f'{img_dir_mod}DMD_fit_{numRows}_{numCols}')
    helper_funcs.make_directory(fit_dir)

    # Calculate row_pixel_lst, col_pixel_lst
    DMD_rows = dmd.numRows
    DMD_cols = dmd.numCols
    row_pixel_lst, col_pixel_lst = helper_funcs.get_rowAndColumnLists(numRows, numCols, DMD_rows=DMD_rows, DMD_cols=DMD_cols)

    # pinhole ROI
    roi_pinhole_in_crop = roi_pinhole-np.array([roi_img[0], roi_img[0], roi_img[2], roi_img[2]])

    # Calculate phases
    population_phaseMap, population_amplitudeMap = \
        calculatePhase.calculatePhase(row_pixel_lst, col_pixel_lst,roi_img, roi_pinhole_in_crop,
                                    img_dir,fit_dir, verbose=verbose)

    # save raw phases locally as txt
    with open(os.path.join(fit_dir, 'rawPhaseMap.txt'), 'wb') as f:
        pickle.dump(population_phaseMap,f)
    with open(os.path.join(fit_dir, 'rawAmplitudeMap.txt'), 'wb') as f:
        pickle.dump(population_amplitudeMap,f)

    # unwrap phase
    row_pixel_lst_fit = np.delete(row_pixel_lst, row_excl)
    col_pixel_lst_fit = np.delete(col_pixel_lst, col_excl)

    phaseMap_to_fit = np.delete(np.delete(population_phaseMap, row_excl, axis=0), col_excl, axis=1)
    unwrapped_phaseMap = calculatePhase.phaseUnwrap2D(row_pixel_lst_fit, col_pixel_lst_fit,  DMD_rows, DMD_cols,
                                                        phaseMap_to_fit, fit_dir, save_row_plots=True)

    # save discrete, unwrapped phase map locally as txt and as png
    with open(os.path.join(fit_dir, 'unwrapped_phaseMap.txt'), 'wb') as f:
        pickle.dump(unwrapped_phaseMap,f)
    calculatePhase.plotAndSaveDiscreteMap(unwrapped_phaseMap,numRows,numCols,fit_dir,'unwrappedPhaseMap_discrete')
    
    # fix up the amplitude map
    amplMap_to_fit = calculatePhase.fixAmplNearRefPatch(population_amplitudeMap)
    amplMap_to_fit = np.delete(np.delete(population_amplitudeMap, row_excl, axis=0), col_excl, axis=1)
    amplMap_to_fit = calculatePhase.GaussFilter(amplMap_to_fit,sigma=1)
   
   # interpolate phase and amplitude maps to full DMD array
    phaseMap_interpolated = calculatePhase.interpolateDiscreteMapAndSavePlt(row_pixel_lst_fit, col_pixel_lst_fit, DMD_rows, DMD_cols,
        unwrapped_phaseMap,fit_dir, 'phaseMap_interpolated')
    amplitudeMap_interpolated = calculatePhase.interpolateDiscreteMapAndSavePlt(row_pixel_lst_fit,col_pixel_lst_fit,DMD_rows, DMD_cols,
        amplMap_to_fit, fit_dir, 'amplitudeMap_interpolated')

    # show plots
    if show_plots[0]:
        show_a_map(population_phaseMap, 'raw phase map')
        show_a_map(unwrapped_phaseMap, 'unwrapped phase map')
    if show_plots[1]:
        show_a_map(phaseMap_interpolated, 'interpolated phase map')
    if show_plots[2]:
        show_a_map(population_amplitudeMap, 'raw amplitude map')
        show_a_map(amplMap_to_fit, 'fixed-up amplitude map')
    if show_plots[3]:
        show_a_map(amplitudeMap_interpolated, 'interpolated amplitude map')

    # save interpolated amplitude and unwrapped-phase maps locally as txt
    with open(os.path.join(fit_dir, 'phaseMap_interpolated.txt'), 'wb') as f:
        pickle.dump(phaseMap_interpolated,f)
    with open(os.path.join(fit_dir, 'amplitudeMap_interpolated.txt'), 'wb') as f:
        pickle.dump(amplitudeMap_interpolated,f)

    print('Done calculating phase!')

    if overwrite_main_maps[0]:
        # overwrite phase map
        print('Overwriting primary phase map...')
        fit_phase_map_name = os.path.join(fit_dir, 'phaseMap_interpolated')
        primary_phase_map_name = os.path.join(helper_funcs.get_main_directory_path(), os.path.join('maps','currentPhaseMap'))
        shutil.copyfile(fit_phase_map_name + '.txt', primary_phase_map_name + '.txt')
        shutil.copyfile(fit_phase_map_name + '.png', primary_phase_map_name + '.png')
    else:
        print('Not overwriting primary phase map.')

    if overwrite_main_maps[1]:
        # overwrite amplitude map
        print('Overwriting primary amplitude map...')
        fit_ampl_map_name = os.path.join(fit_dir, 'amplitudeMap_interpolated')
        primary_ampl_map_name = os.path.join(helper_funcs.get_main_directory_path(), os.path.join('maps','currentAmplMap'))
        shutil.copyfile(fit_ampl_map_name + '.txt', primary_ampl_map_name + '.txt')
        shutil.copyfile(fit_ampl_map_name + '.png', primary_ampl_map_name + '.png')
    else:
        print('Not overwriting primary amplitude map.')

    return unwrapped_phaseMap

def show_a_map(the_map, the_title):
    plt.imshow(the_map, interpolation='nearest')
    plt.colorbar()
    plt.title(the_title)
    plt.xlabel('column #')
    plt.ylabel('row #')
    plt.show()
    plt.close()